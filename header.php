<!DOCTYPE html>
<html lang="pt-BR">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" type="text/css" href="<?php echo get_stylesheet_directory_uri()?>/style.css">
    <title><?php bloginfo("name") ?></title>
    <?php wp_head(); ?>
</head>
<body>

<?php $image_header = get_field('imagem-logo')?>

    <header id="header">
        <div class="img-header">
            <img src="<?php echo $image_header?>" width="200" heigth="200">
        </div>   
    </header>