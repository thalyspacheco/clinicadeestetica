<?php
// Template Name: home
?>
<?php get_header();?>
<?php home(); ?>
    <main>
        
            <div class="sobre">
                <div>
                    <h1><?php the_field('sobre-titulo'); ?></h1>
                </div>
                <div class="decricao">
                    <p class="descricao"><?php the_field('sobre-descricao'); ?>
                    </p>
                </div>
            </div>
        
        <section class="medicos">
            <h2>MÉDICOS</h2>
            <div class="carrossel">
                <?php echo do_shortcode('[psac_post_slider show_date="false" show_author="false" show_comments="false" show_category="true" show_content="true" show_read_more="false" sliderheight="1800] '); ?>
            </div>
            
        </section>
    </main>
    
<?php get_footer();?>