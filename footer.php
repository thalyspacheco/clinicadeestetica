<?php
// Template Name: footer
?>
<footer>
        <div id="container-footer">

            <div id="content-footer">
              <div id="contato">
                <div class="content-contato"><img class="img-footer" src="<?php echo get_stylesheet_directory_uri()?>/Instagram.png"><p><?php the_field('contato-insta'); ?></p></div>
                <div class="content-contato"><img class="img-footer wpp" src="<?php echo get_stylesheet_directory_uri()?>/telefone.png"><p><?php the_field('contato-numero'); ?></p></div>
                <div class="content-contato"><img class="img-footer" src="<?php echo get_stylesheet_directory_uri()?>/email.png"><p><?php the_field('contato-email'); ?></p></div>
              </div>
              <div id="mapa">
                <?php the_field('mapa'); ?>
              </div>
            </div>

            <div id="copyright">
              <div id="content-copyright">
                <p>copyright &#0169; Nome da clinica 2022 - todos os direitos reservados</p>
              </div>
            </div>
            
        </div>
    </footer>
    <?php wp_footer(); ?>
  </body>
</html>