<?php
// Template Name: medicos
?>
<?php get_header();?>
    <main id="pag-med">
        <div id="sobre-medico">

            <div id="informacoes-medico">
                <img class="img-med" src="<?php echo get_stylesheet_directory_uri()?>/anonima.png" alt="">
                <h1 class="nome-med">Nome do medico</h1>
                <h2 class="esp-med">Especialidade</h2>
            </div>

            <div id="texto-medico">
                <p class="p-med">Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore
et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa
qui officia deserunt mollit anim id est laborum.</p>
            </div>

        </div>

        <div id="portifolio">
            <h1>PORTIFOLIO</h1>
            <div id="img-portifolio">

                <div class="caixa-port">
                    <img class="img-port" src="" alt="">
                    <div class="name-port">
                        <h2>NOME PORTIFOLIO</h2>
                    </div>
                </div>
                
                <div class="caixa-port">
                    <img class="img-port" src="" alt="">
                    <div class="name-port">
                        <h2>NOME PORTIFOLIO</h2>
                    </div>
                </div>
                
                <div class="caixa-port">
                    <img class="img-port ult-img-port" src="" alt="">
                    <div class="name-port ult-name-port">
                        <h2>NOME PORTIFOLIO</h2>
                    </div>
                </div>

            </div>

        </div>
        <div id="agenda">
            <h1>AGENDA</h1>
            <div id="content-agenda">
                <?php the_content();?>
            </div>
        </div>
    </main>
<?php get_footer();?>