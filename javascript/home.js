// Ajustar href que leva para cada médico no carrossel
linkNode = document.querySelectorAll('.psac-link-overlay')
doctorBoxNode = document.querySelectorAll('.psac-post-overlay')
doctorNode = document.querySelectorAll('.psac-post-categories')
slidesNode = document.querySelectorAll('.psac-post-slides')
doctorNameNode = document.querySelectorAll('.psac-post-title')
postContentNode = document.querySelectorAll('.psac-post-content')

const links = Array.from(linkNode)
const doctors = Array.from(doctorNode)
const slides = Array.from(slidesNode)
const doctorBox = Array.from(doctorBoxNode)
const doctorName = Array.from(doctorNameNode)
const postContent = Array.from(postContentNode)

links.forEach(div => {
  destination = div.nextSibling.nextSibling.nextElementSibling
  destination.appendChild(div)
})

// Ajustando disposição de divs no carrossel
createDiv()
function createDiv() {
  doctorBox.forEach(card => {
    div = document.createElement('div')
    div.classList.add('name-category')
    card.appendChild(div)
  })

  doctors.forEach(category => {
    destination = category.parentElement.lastElementChild
    destination.appendChild(category)
  })

  doctorName.forEach(name => {
    destination = name.parentElement.lastElementChild
    destination.appendChild(name)
  })

  // criando outra div pra descriçao do medico dentro de name-category div

  nameCategoriesNode = document.querySelectorAll('.name-category')
  nameCategories = Array.from(nameCategoriesNode)

  nameCategories.forEach(newDiv => {
    divDescription = document.createElement('div')
    divDescription.classList.add('doctor-description')
    newDiv.appendChild(divDescription)
  })
  postContent.forEach(content => {
    destination = content.parentElement.lastElementChild.lastElementChild
    destination.appendChild(content)
  })

  // criando div pra receber a imagem do medico passada via custom field no post

  doctorBox.forEach(image => {
    divImage = document.createElement('div')
    divImage.classList.add('doctor-image')

    image.appendChild(divImage)
  })

  // pegando ID do post relacionado ao medico

  return
}
